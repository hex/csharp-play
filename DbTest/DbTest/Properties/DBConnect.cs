﻿using System;
using MySql.Data.MySqlClient;

namespace DbTest
{
	public class DBConnect
	{
		private MySqlConnection connection;
		private string server;
		private string database;
		private string uid;
		private string password;

		public DBConnect()
		{
			Initialize();
		}

		private void Initialize()
		{
			server = "db3.anpro21.com";
			database = "anpro21";
			uid = "db_trainingdo";
			password = "bR8Zsu22zEpdCSAs";
			string connectionString;
			connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
			connection = new MySqlConnection(connectionString);
		}

		private bool OpenConnection()
		{
			try
			{
				connection.Open();
				return true;
			}
			catch (MySqlException ex)
			{
				switch (ex.Number)
				{
				case 0:
					Console.WriteLine("Cannot connect to server.  Contact administrator");
					break;

				case 1045:
					Console.WriteLine("Invalid username/password, please try again");
					break;
				}
				return false;
			}
		}

		private bool CloseConnection()
		{
			try
			{
				connection.Close();
				return true;
			}
			catch (MySqlException ex)
			{
				Console.WriteLine(ex.ToString);
				return false;
			}
		}

		public void Insert()
		{
			string query = "INSERT INTO tableinfo (name, age) VALUES('John Smith', '33')";
			if (this.OpenConnection() == true)
			{
				MySqlCommand cmd = new MySqlCommand(query, connection);
				cmd.ExecuteNonQuery();
				this.CloseConnection();
			}
		}

		public void Update()
		{
			string query = "UPDATE tableinfo SET name='Joe', age='22' WHERE name='John Smith'";
			if (this.OpenConnection() == true)
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.CommandText = query;
				cmd.Connection = connection;
				cmd.ExecuteNonQuery();
				this.CloseConnection();
			}
		}

		public void Delete()
		{
			string query = "DELETE FROM tableinfo WHERE name='John Smith'";

			if (this.OpenConnection() == true)
			{
				MySqlCommand cmd = new MySqlCommand(query, connection);
				cmd.ExecuteNonQuery();
				this.CloseConnection();
			}
		}



	}
}

